<?php

use \Pondit\Calculator\NumberCalculator\Subtraction;
use \Pondit\Calculator\NumberCalculator\Displayer;
use \Pondit\Request;

include_once "vendor/autoload.php";

$number1=0;
$number2=0;
$request= new Request($_REQUEST);
//creating a new object($addition1) from class (Addition)
$subtraction1 = new Subtraction();
$subtraction1->number1=$request->getNumber('number1');
$subtraction1->number2=$request->getNumber("number2");
$result = "The subtraction of two number is: " . $subtraction1->subtract2numbers();

$displayer = new Displayer();
$displayer->displaysimple($result);
$displayer->displaypre($result);
