<?php
use \Pondit\Calculator\NumberCalculator\Addition;
use \Pondit\Calculator\NumberCalculator\Displayer;
use \Pondit\Request;

include_once "vendor/autoload.php";
$number1=0;
$number2=0;
$request= new Request($_REQUEST);

//creating a new object($addition1) from class (Addition)
$addition1 = new Addition();
$addition1->number1=$request->getNumber('number1');
$addition1->number2=$request->getNumber('number2');

$result = "The addition of two number is: " . $addition1->add2numbers();
$displayer = new Displayer();
$displayer->displaysimple($result);
$displayer->displaypre($result);
