<?php
use \Pondit\Calculator\NumberCalculator\Divission;
use \Pondit\Calculator\NumberCalculator\Displayer;
use \Pondit\Request;

include_once "vendor/autoload.php";
$number1=0;
$number2=0;
$request= new Request($_REQUEST);

//creating a new object($addition1) from class (Addition)
$divission1 = new Divission();
$divission1->number1=$request->getNumber('number1');
$divission1->number2=$request->getNumber('number2');
$result = "The divission of two number is: " . $divission1->divide2numbers();

$displayer = new Displayer();
$displayer->displaysimple($result);
$displayer->displaypre($result);