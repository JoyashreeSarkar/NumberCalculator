<?php
/**
 * Created by PhpStorm.
 * User: DESH-IT
 * Date: 4/5/2018
 * Time: 6:11 PM
 */

namespace Pondit;


class Request
{
    public $get = [];
    public $post = [];
    public $request=[];

    public function __construct($request){
        $this->request=$request;
    }
    public function getNumber($name){
        if($this->has($name)&& $this->isNotEmpty($name)){
            return $this->request[$name];
        }
        return 0;

    }

    public function has($name){
        if(array_key_exists($name, $this->request)){
           // $number1=$_POST['number1'];
            return true;
        }
        return false;
    }
    public function isEmpty($value){
        return empty($value);
    }
    public function isNotEmpty($value){
        return !empty($value);
    }

}