<?php
use \Pondit\Calculator\NumberCalculator\Multiplication;
use \Pondit\Calculator\NumberCalculator\Displayer;
use \Pondit\Request;
include_once "vendor/autoload.php";

$number1=0;
$number2=0;

$request= new Request($_REQUEST);
//creating a new object($addition1) from class (Addition)
$multiplication1=new Multiplication();
$multiplication1->number1=$request->getNumber('number1');
$multiplication1->number2=$request->getNumber('number2');
$result="The multiplication of two number is: ".$multiplication1->multiple2numbers();

$displayer= new Displayer();
$displayer->displaysimple($result);
$displayer->displaypre ($result);
